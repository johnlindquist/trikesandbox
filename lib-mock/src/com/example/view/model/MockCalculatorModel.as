/**
 * User: John Lindquist
 * Date: 11/7/11
 * Time: 11:40 AM
 */
package com.example.view.model {
    import com.example.dto.CalculationResult;

    [Bindable]
    public class MockCalculatorModel extends CalculatorModel {
        public function MockCalculatorModel() {
        }

        /**
         * DEV-GUIDE: When mocking, pass your fake results to the handlers instead of settings properties directly.
         * This helps to make sure your handler logic is intact.
         */
        override public function doCalculation():void {
            var calculationResult:Number;
            switch(operators.getItemAt(selectedOperator)) {
                case "+":
                    calculationResult = Number(firstNumber) + Number(secondNumber);
                    handleGetAdditionResult(new CalculationResult(calculationResult), "");
                    break;
                case "-":
                    calculationResult = Number(firstNumber) - Number(secondNumber);
                    handleGetAdditionResult(new CalculationResult(calculationResult), "");
                    break;
                case "*":
                    calculationResult = Number(firstNumber) * Number(secondNumber);
                    handleGetAdditionResult(new CalculationResult(calculationResult), "");
                    break;
                case "/":
                    calculationResult = Number(firstNumber) / Number(secondNumber);
                    handleGetAdditionResult(new CalculationResult(calculationResult), "");
                    break;
            }
        }
    }
}
