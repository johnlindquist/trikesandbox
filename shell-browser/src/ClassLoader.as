/**
 * User: John Lindquist
 * Date: 10/26/11
 * Time: 2:31 PM
 */
package {
public class ClassLoader {
    private var classes:Array = new Array();

    public function ClassLoader() {
        include "classLoaders/serviceClassLoader.as"
        include "classLoaders/signalClassLoader.as"
        include "classLoaders/mockClassLoader.as"
        for (var i:int = 0; i < classes.length; i++) {
            var object:Object = classes[i];
            trace(object);
        }
    }
}
}
