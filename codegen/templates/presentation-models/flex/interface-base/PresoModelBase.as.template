##------------------------------------------------------------------------
##
##  Velocity Variables
##
##------------------------------------------------------------------------
##
#setPresentationModelVars( $data )
##
##----------------------------------
##  The file type extension
##----------------------------------
##
#set( $extension = "as" )##
##
##
##----------------------------------
##  The derived package name for
##  this Event class
##----------------------------------
##
#set( $packageName = "${data.packageName}.base" )
##
##
##----------------------------------
##  The derived class name for
##  this Event class
##----------------------------------
##
#set( $className = "${data.name}Base" )
##
#set( $rewrite = "true" )
##
##
##------------------------------------------------------------------------
##
##  ActionScript Source
##
##------------------------------------------------------------------------
##
#writeHeader( $rewrite )

package $packageName
{

import org.trikeframework.model.PresentationModel;

#foreach ( $import in ${data.imports} )##
#setImportVars( $import )##
import $import;
#end##

#foreach ( $call in ${data.calls} )##
#setCallVars( $call )##
#set( $signalPackage = "${basePackage}.${signalsPackage}.${stringHelper.toLower( ${call} )}" )
#set( $opName = "${stringHelper.firstLetterUpper( ${call}, false )}" )
#set( $OpName = ${stringHelper.firstLetterUpper( ${call}, false )} )##
#set( $requestSignalName = "Signal${OpName}Request" )
import ${signalPackage}.${requestSignalName};
import ${signalPackage}.${OpName}RequestVO;
#end##

#foreach ( $service in ${services} )##
#setServiceVars( $service )##
#**##foreach ( $op in ${service.operations} )##
#*  *##setServiceOperationVars( $op )##
#*  *##foreach ( $call in ${data.calls} )##
#*      *##setCallVars( $call )##
#*      *##setTypeVars( ${op.returnType} )##
#*      *##if( ${stringHelper.equals( "${call}", "${op.name}" )} && "${typePackage}" != "" && ${typeType} != "Array" )##
import ${typePackage}.base.${typeType}Base;
#*  	*##end##
#*  *##end##
#**##end##
#end##

import mx.rpc.Fault;


[Bindable]
/**
 *  ${data.comment}##
 */
public class $className extends PresentationModel implements I${data.name}
{
##    //--------------------------------------------------------------------------
##    //
##    //  Property name constants
##    //
##    //--------------------------------------------------------------------------
##
###foreach ( $service in ${services} )##
#setServiceVars( $service )##
###**##foreach ( $op in ${service.operations} )##
#*  *##setServiceOperationVars( $op )##
###*  *##foreach ( $call in ${data.calls} )##
###*    *##setCallVars( $call )##
###*    *##if( ${stringHelper.equals( "${call}", "${op.name}" )} )##
##	public static const PROP_${stringHelper.toUpperWithUnderscores( $call )}_RESULT : String = "${call}Result";
###*    *##end##
###*  *##end##
###**##end##
###end##
###foreach ( $type in ${data.properties} )##
###setTypeVars( $type )##
##	public static const PROP_${stringHelper.toUpperWithUnderscores( ${type.name} )} : String = "${type.name}";
###end##
##

    //--------------------------------------------------------------------------
    //
    //  Properties
    //
    //--------------------------------------------------------------------------

#foreach ( $type in ${data.properties} )##
#setTypeVars( $type )##
	//----------------------------------
	//  ${type.name}
	//----------------------------------

#if( ${stringHelper.contains( "${typeType}", "Signal" )} )##
	[Inject]
#end##
	/**
#*	*##${type.comment}
	 */
	public function get ${type.name}() : ${typeType} {
		return __${type.name};
	}

	public function set ${type.name}( val : ${typeType} ) : void {
		__${type.name} = val;
	}

	private var __${type.name} : ${typeType};

#end##

    //--------------------------------------------------------------------------
    //
    //  Result properties
    //
    //--------------------------------------------------------------------------
#foreach ( $service in ${services} )##
#setServiceVars( $service )##
#**##foreach ( $op in ${service.operations} )##
#*  *##setServiceOperationVars( $op )##
#*  *##foreach ( $call in ${data.calls} )##
#*      *##setTypeVars( ${op.returnType} )##
#*      *##setCallVars( $call )##
#*      *##if( ${stringHelper.equals( "${call}", "${op.name}" )} )##
#*          *##if( ${typeType} == "void" )##
#*              *##set( $typeType = "Object" )##
#*          *##end##

	/**
	 *  Result property for ${call} call.
	 */
	public function get ${call}Result() : ${typeType}Base {
		return __${call}Result;
	}

	public function set ${call}Result( val : ${typeType}Base ) : void {
		__${call}Result = val;
	}

	private var __${call}Result : ${typeType}Base;
#*      *##end##
#*  *##end##
#**##end##
#end##

    //--------------------------------------------------------------------------
    //
    //  Signals
    //
    //--------------------------------------------------------------------------
#foreach ( $call in ${data.calls} )##
#setCallVars( $call )##
#set( $signalPackage = "${basePackage}.${signalsPackage}.${stringHelper.toLower( ${call} )}" )
#set( $opName = "${stringHelper.firstLetterUpper( ${call}, false )}" )
#set( $OpName = ${stringHelper.firstLetterUpper( ${call}, false )} )##
#set( $requestSignalName = "Signal${OpName}Request" )
#set( $Call = ${stringHelper.firstLetterUpper( ${call}, false )} )##
	/**
	 *  Request signal for ${call} call.
	 */
	[Inject]
	public function set ${call}Request( val : ${requestSignalName} ) : void {
		__${call}Request = val;
	}

	public function get ${call}Request() : ${requestSignalName} {
		return __${call}Request;
	}

	private var __${call}Request : ${requestSignalName};

	/**
	 *  @protected
	 *
#*   *#${stringHelper.wrapAsDocStyleComment( "Proxy to ${call}Request.dispatchAndRespond() that wires the result to handle${Call}Result", 80, 1 )}
	 */
	protected function dispatch${Call}Request( params : ${Call}RequestVO ) : void {
		loading = true;
		addOutStandingCallUID(${call}Request.dispatchAndRespond( params, handle${Call}Result, handle${Call}Fault ));
	}

#end##


    //--------------------------------------------------------------------------
    //
    //  Result Handlers
    //
    //--------------------------------------------------------------------------
#foreach ( $service in ${services} )##
#setServiceVars( $service )##
#**##foreach ( $op in ${service.operations} )##
#*  *##setServiceOperationVars( $op )##
#*  *##foreach ( $call in ${data.calls} )##
#*      *##setCallVars( $call )##
#*      *##setTypeVars( ${op.returnType} )##
#*      *##if( ${stringHelper.equals( "${call}", "${op.name}" )} )##
#*  	   *##set( $Call = ${stringHelper.firstLetterUpper( ${call}, false )} )##
##
#*      *##if( ${typeType} == "void" )##
#*          *##set( $typeType = "Object" )##
#*      *##end##

	/**
	 *  Result handler for ${call} call.
	 */
	protected function handle${Call}Result( result : ${typeType}Base, uid : String ) : void {
		${call}Result = result;
		removeOutStandingCallUID(uid);
		loading = false;
	}

	/**
	 *  Fault handler for ${call} call.
	 */
	protected function handle${Call}Fault( fault : Fault, uid : String ) : void {
		removeOutStandingCallUID(uid);
		loading = false;
	}
#*      *##end##
#*  *##end##
#**##end##
#end##


    //--------------------------------------------------------------------------
    //
    //  Methods
    //
    //--------------------------------------------------------------------------

#foreach ( $operation in ${data.methods} )##
#setMethodVars( $operation )##
	//----------------------------------
	//  ${operation.name}
	//----------------------------------

	/**
#*	*##${operation.comment}
	 */
	public function ${operation.name}( ##
#foreach ( $type in ${operation.parameters} )##
#setTypeVars( $type )##
#*	*#${type.name} : ${typeType}##
#*	*##isNotLastWriteComma( ${operation.parameters} )##
#*	*# ##
#end##
#*		*#) : ${operationReturnTypeType} {
#*		*##if( ${operationReturnTypeType} != "void" && ${operationReturnTypeType} != "int" && ${operationReturnTypeType} && ${operationReturnTypeType} != "Number")##
			return null;
#*  	*##end##
#*		*##if( ${operationReturnTypeType} == "int" || ${operationReturnTypeType} == "Number")##
			return NaN;
#*  	*##end##
	}
#end##

} //  end class
} //  end package
