/**
 * User: John Lindquist
 * Date: 10/25/11
 * Time: 4:09 PM
 */
package com.example.view.base {
import com.example.view.model.CalculatorModel;

import org.trikeframework.view.BaseView;

import spark.layouts.VerticalLayout;

    /**
     * DEV-GUIDE: Each View will have a supporting "BaseView". You create this as a simple entry point for Injection and
     * basic layout setup.
     */
public class CalculatorBaseView extends BaseView {

    public function CalculatorBaseView() {
        layout = new VerticalLayout();
    }

//  DEV-GUIDE  The model is injected into the BaseView of each View implementation
    [Inject]
    [Bindable]
    public var model:CalculatorModel;
}
}
